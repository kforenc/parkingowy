package com.kamel.parkingowy.core.msg;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import static junitparams.JUnitParamsRunner.$;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class ParkingStatusExtractorTest {
    private DefaultParkingStatusExtractor extractor;

    @Before
    public void setUp() throws Exception {
        extractor = new DefaultParkingStatusExtractor();
    }

    @Test
    @Parameters(method = "getParams")
    public void shouldProperlyIdentifyStartMessage(String content, ParkingStatus expectedStatus) {
        assertThat(extractor.extractStatusFromMsg(new MobiParkingMessageContent(content, new Date(0))), is
                (expectedStatus));
    }

    public Object[] getParams() {
        return $(
                $("Rozpoczeto parkowanie PS222AF w strefie Krakow. Saldo: 44.33 PLN", ParkingStatus.active()),
                $("Rozpoczeto parkowanie ps222xl w strefie Poznan. Saldo: 4.20 PLN", ParkingStatus.active()),
                $("Rozpoczeto parkowanie ps222xl w strefie Poznan. Saldo: 8.00 PLN", ParkingStatus.active()),
                $("Rozpoczeto parkowanie po88821 w strefie Krakow. Saldo: 12.23 PLN", ParkingStatus.active()),
                $("Rozpoczeto parkowanie kns89212 w strefie Krakow. Saldo: 12,23 PLN", ParkingStatus.active()),
                $("Rozpoczeto2 parkowanie po88821 w strefie Krakow. Saldo: 12.23 PLN", ParkingStatus.inactive()),
                $("Zakonczono parkowanie ps222xl w strefie Poznan. Saldo: 8.00 PLN", ParkingStatus.inactive())
        );
    }
}