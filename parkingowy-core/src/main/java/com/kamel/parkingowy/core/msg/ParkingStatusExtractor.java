package com.kamel.parkingowy.core.msg;

/**
 * @author: Kamil Forenc
 */
public interface ParkingStatusExtractor {
    ParkingStatus extractStatusFromMsg(MobiParkingMessageContent content);
}
