package com.kamel.parkingowy.core.msg;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: Kamil Forenc
 */
public class ParkingStatus {

    private final boolean active;
    private final Date startDate;
    private final BigDecimal startAmount;
    private final String carRegistrationNumber;

    public static ParkingStatus inactive() {
        return new ParkingStatus(false);
    }

    public static ParkingStatus active() {
        return new ParkingStatus(true);
    }

    private ParkingStatus(boolean active, String carRegistrationNumber, BigDecimal startAmount, Date startDate) {
        this.active = active;
        this.carRegistrationNumber = carRegistrationNumber;
        this.startAmount = startAmount;
        this.startDate = startDate;
    }

    private ParkingStatus(boolean active) {
        this(active, "", BigDecimal.ZERO, new Date(0));
    }

    public boolean isActive() {
        return active;
    }

    public String getCarRegistrationNumber() {
        return carRegistrationNumber;
    }

    public BigDecimal getStartAmount() {
        return startAmount;
    }

    public Date getStartDate() {
        return startDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParkingStatus status = (ParkingStatus) o;

        if (active != status.active) return false;
        if (!startDate.equals(status.startDate)) return false;
        if (!startAmount.equals(status.startAmount)) return false;
        return carRegistrationNumber.equals(status.carRegistrationNumber);

    }

    @Override
    public int hashCode() {
        int result = (active ? 1 : 0);
        result = 31 * result + startDate.hashCode();
        result = 31 * result + startAmount.hashCode();
        result = 31 * result + carRegistrationNumber.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ParkingStatus{" +
                "active=" + active +
                ", startDate=" + startDate +
                ", startAmount=" + startAmount +
                ", carRegistrationNumber='" + carRegistrationNumber + '\'' +
                '}';
    }
}
