package com.kamel.parkingowy.core.msg;

import java.util.Date;

/**
 * @author: Kamil Forenc
 */
public class MobiParkingMessageContent {

    private final String msg;
    private final Date createDate;

    public MobiParkingMessageContent(String msg, Date createDate) {
        this.msg = msg;
        this.createDate = createDate;
    }

    public String getMsg() {
        return msg;
    }

    public Date getCreateDate() {
        return createDate;
    }
}
