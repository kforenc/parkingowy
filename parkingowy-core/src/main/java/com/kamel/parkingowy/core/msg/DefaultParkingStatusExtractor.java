package com.kamel.parkingowy.core.msg;

import java.util.regex.Pattern;

/**
 * @author: Kamil Forenc
 */
public class DefaultParkingStatusExtractor implements ParkingStatusExtractor {

    public static Pattern PARKING_START_MSG_PATTERN = Pattern.compile("Rozpoczeto parkowanie ([\\w\\d]+) w strefie " +
            "([\\w]+)\\. Saldo: (\\d+[\\.,]\\d{2}) PLN");

    @Override
    public ParkingStatus extractStatusFromMsg(MobiParkingMessageContent msg) {
        if (msg == null || !PARKING_START_MSG_PATTERN.matcher(msg.getMsg()).matches()) {
            return ParkingStatus.inactive();
        }

        return ParkingStatus.active();
    }
}
