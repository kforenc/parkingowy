package com.kamel.parkingowy.app.service;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import com.kamel.parkingowy.core.msg.MobiParkingMessageContent;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: Kamil Forenc
 */
public class MobiParkingMessageContentProvider {

    public static final String CONTENT_SMS_INBOX_URI = "content://sms/inbox";
    public static final String ADDRESS = "address";
    public static final String BODY = "body";
    public static final String ORDER_BY_DATE_DESC = "date DESC";
    public static final String MOBI_PARKING_SMS_ADDRESS = "82002";
    public static final String DATE = "date";

    public MobiParkingMessageContent provideMessageContent(ContentResolver contentResolver) {
        Uri uri = Uri.parse(CONTENT_SMS_INBOX_URI);
        Cursor cursor = null;
        try {
            cursor = contentResolver.query(uri, new String[]{ADDRESS, BODY, DATE}, String.format("%s = ?", ADDRESS), new
                    String[]{MOBI_PARKING_SMS_ADDRESS}, ORDER_BY_DATE_DESC);

            if (cursor.moveToNext()) {
                return new MobiParkingMessageContent(cursor.getString(1), parseDate(cursor));
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return null;
    }

    private Date parseDate(Cursor cursor) {
        try {
            return new SimpleDateFormat().parse(cursor.getString(2));
        } catch (ParseException e) {
            return new Date();
        }
    }
}
