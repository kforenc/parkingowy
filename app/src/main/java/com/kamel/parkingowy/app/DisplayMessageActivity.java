package com.kamel.parkingowy.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.widget.TextView;


public class DisplayMessageActivity extends ActionBarActivity {

    public static final int REQUEST_CODE = 85462548;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Create the text view
        TextView textView = new TextView(this);
        textView.setTextSize(40);
        textView.setText("wiad: " + getLastMessage());

        // Set the text view as the activity layout
        createNotification(message);

        setContentView(textView);
    }

    private String getLastMessage() {

        Uri uri = Uri.parse("content://sms/inbox");
        Cursor c = null;
        try {
            c = getContentResolver().query(uri, new String[]{"address", "body"}, "address = ?", new
                    String[]{"82002"}, "date DESC");


            if (c.moveToNext()) {
                return c.getString(1);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return "0";
    }

    private void createNotification(String message) {
        Notification notification = new Notification(R.drawable.ic_stat_notify_parking_on, "Parking włączony",
                System.currentTimeMillis());

        notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        Context context = getApplicationContext();
        Intent i = new Intent(this, MainActivity.class);
        PendingIntent pd = PendingIntent.getActivity(this, REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setLatestEventInfo(context, "Parking", "Opłacany w trybie ciągłym", pd);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (message.startsWith("ko")) {
            notificationManager.cancel(REQUEST_CODE);
        } else {
            notificationManager.notify(REQUEST_CODE, notification);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
