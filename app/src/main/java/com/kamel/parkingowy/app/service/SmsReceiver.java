package com.kamel.parkingowy.app.service;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.kamel.parkingowy.app.service.MobiParkingMessageContentProvider.MOBI_PARKING_SMS_ADDRESS;
import static com.kamel.parkingowy.app.service.ParkingStatusService.EXTRA_MSG_CONTENT;
import static com.kamel.parkingowy.app.service.ParkingStatusService.EXTRA_MSG_DATE;

/**
 * @author: Kamil Forenc
 */
public class SmsReceiver extends BroadcastReceiver {

    private static final Set<String> SMS_ADDRESSES = new HashSet(Arrays.asList(
            MOBI_PARKING_SMS_ADDRESS, "0048" + MOBI_PARKING_SMS_ADDRESS));

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                if (smsFromMobiParking(smsMessage)) {
                    Intent serviceIntent = new Intent(context, ParkingStatusService.class);
                    serviceIntent.putExtra(EXTRA_MSG_CONTENT, smsMessage.getMessageBody());
                    serviceIntent.putExtra(EXTRA_MSG_DATE, smsMessage.getTimestampMillis());
                    context.startService(serviceIntent);
                }
            }
        }
    }

    private boolean smsFromMobiParking(SmsMessage smsMessage) {
        return SMS_ADDRESSES.contains(smsMessage.getOriginatingAddress());
    }
}
