package com.kamel.parkingowy.app.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.kamel.parkingowy.app.MainActivity;
import com.kamel.parkingowy.app.R;
import com.kamel.parkingowy.core.msg.DefaultParkingStatusExtractor;
import com.kamel.parkingowy.core.msg.MobiParkingMessageContent;
import com.kamel.parkingowy.core.msg.ParkingStatus;
import com.kamel.parkingowy.core.msg.ParkingStatusExtractor;

import java.util.Date;

/**
 * @author: Kamil Forenc
 */
public class ParkingStatusService extends IntentService {

    public static final String THREAD_NAME = "ParkingowySMSUpdateThread";
    public static final String EXTRA_MSG_CONTENT = "com.kamel.parkingowy.app.MSG_CONTENT";
    public static final String EXTRA_MSG_DATE = "com.kamel.parkingowy.app.MSG_DATE";
    public static final int REQUEST_CODE = 85462548;
    private ParkingStatusExtractor extractor = new DefaultParkingStatusExtractor();
    private MobiParkingMessageContentProvider provider = new MobiParkingMessageContentProvider();

    public ParkingStatusService() {
        super(THREAD_NAME);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ParkingStatus status = extractor.extractStatusFromMsg(getMessageContent(intent));
        if (status.isActive()) {
            makeNotification();
        } else {
            cancelNotification();
        }
    }

    private MobiParkingMessageContent getMessageContent(Intent intent) {
        if (intentContainsMessageContentData(intent)) {
            return new MobiParkingMessageContent(intent.getStringExtra(EXTRA_MSG_CONTENT), new Date(intent
                    .getLongExtra(EXTRA_MSG_DATE, 0L)));
        }

        return provider.provideMessageContent(getContentResolver());
    }

    private boolean intentContainsMessageContentData(Intent intent) {
        return intent.hasExtra(EXTRA_MSG_CONTENT) && intent.hasExtra(EXTRA_MSG_DATE);
    }

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(REQUEST_CODE);
    }

    private void makeNotification() {
        Notification notification = new Notification(R.drawable.ic_stat_notify_parking_on, "Parking w��czony",
                System.currentTimeMillis());

        notification.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pd = PendingIntent.getActivity(this, REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.setLatestEventInfo(getApplicationContext(), "Parking", "Oplacany w trybie ciaglym", pd);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(REQUEST_CODE, notification);
    }
}
